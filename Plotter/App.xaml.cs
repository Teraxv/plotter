﻿using Plotter.Common.Utils;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Threading;

namespace Plotter.PlotterGUI
{
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            // load 
            Current.Resources.MergedDictionaries.Add(new ResourceDictionary() { Source = new Uri("/Common;component/Generic.xaml", UriKind.Relative) });

            // handle exceptions
            Current.DispatcherUnhandledException += Current_DispatcherUnhandledException;
            Current.ShutdownMode = ShutdownMode.OnMainWindowClose;

            // run main window
            var mainWindow = new MainWindow
            {
                DataContext = new MainWindowViewModel()
            };
            MainWindow.Show();
        }

        private void Current_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            WindowHelper.ShowWarning(e.Exception.Message);

            // gracefuly handle the exception if not in debug mode
            e.Handled = !Debugger.IsAttached;
        }
    }
}
