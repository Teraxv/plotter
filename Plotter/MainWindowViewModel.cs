﻿using OxyPlot;
using Plotter.Common.Controls;
using Plotter.Common.ViewModels;
using Plotter.Core.Controls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace Plotter.PlotterGUI
{
    public class MainWindowViewModel : ViewModelBase
    {
        private readonly PlotTabViewModel plotTab;
        private readonly ImportTabViewModel importTab;
        private readonly AboutTabViewModel aboutTab;

        public MainWindowViewModel()
        {
            AvailableTabs = new ObservableCollection<PanelTabViewModelBase>();
            AvailableTabs.Add(plotTab = new PlotTabViewModel());
            AvailableTabs.Add(importTab = new ImportTabViewModel(plotTab.SeriesManager));
            AvailableTabs.Add(aboutTab = new AboutTabViewModel());
            ActiveTab = AvailableTabs.First();
        }

        public ObservableCollection<PanelTabViewModelBase> AvailableTabs { get; }

        private PanelTabViewModelBase activeTab;
        public PanelTabViewModelBase ActiveTab
        {
            get { return activeTab; }
            set
            {
                if (!AvailableTabs.Contains(value))
                    throw new InvalidOperationException();
                Set(ref activeTab, value);
            }
        }

        private ICommand changeTabCommand;
        public ICommand ChangeTabCommand
        {
            get
            {
                return changeTabCommand ?? (changeTabCommand = new BasicCommand<PanelTabViewModelBase>((newTab) =>
                {
                    if(newTab is null)
                        throw new InvalidOperationException();
                    if (newTab.Equals(activeTab))
                        return;
                    ActiveTab = newTab;
                }));
            }
        }
    }
}
