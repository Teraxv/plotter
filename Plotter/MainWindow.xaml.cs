﻿using System.Windows;

namespace Plotter.PlotterGUI
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            Closing += MainWindow_Closing;
        }

        private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // you want to exit?
            e.Cancel = false;
        }
    }
}
