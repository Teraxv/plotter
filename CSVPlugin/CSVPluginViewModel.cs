﻿using Microsoft.Win32;
using Plotter.Core.Controls;
using Plotter.Core.Models;
using Plotter.Core.Plugins;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Input;

namespace Plotter.CSVPlugin
{
    public class CSVPluginViewModel : ViewModelBase, IPlugin
    {
        private ISeriesManager seriesManager;

        public CSVPluginViewModel()
        {
        }

        public string Name => "CSV Import";

        private string seriesName;
        public string SeriesName
        {
            get { return seriesName; }
            set { Set(ref seriesName, value); }
        }

        private string filePath;
        public string FilePath
        {
            get { return filePath; }
            set { Set(ref filePath, value); }
        }

        private ICommand browseCommand;
        public ICommand BrowseCommand
        {
            get
            {
                return browseCommand ?? (browseCommand = new BasicCommand(() =>
                {
                    // set file path
                    var fileDialog = new OpenFileDialog
                    {
                        DefaultExt = ".csv",
                        Filter = "CSV Files (*.csv)|*.csv|TXT Files (*.txt)|*.txt"
                    };

                    var result = fileDialog.ShowDialog();
                    if (result.HasValue && result.Value)
                        FilePath = fileDialog.FileName;
                }));
            }
        }

        private ICommand addCommand;
        public ICommand AddCommand
        {
            get
            {
                return addCommand ?? (addCommand = new BasicCommand(() =>
                {
                    if (string.IsNullOrWhiteSpace(FilePath) || !File.Exists(FilePath)
                        || string.IsNullOrWhiteSpace(seriesName) || seriesManager.Contains(seriesName))
                        return;

                    try
                    {
                        var data = GetCSVData();

                        if (!data.Any())
                            return;

                        seriesManager.AddSeries(seriesName, data);
                        FilePath = string.Empty;
                        SeriesName = string.Empty;
                    }
                    catch (InvalidFileException)
                    {
                        // we should display proper message here
                    }
                }));
            }
        }

        private IEnumerable<Point> GetCSVData()
        {
            using (var sr = new StreamReader(FilePath))
            {
                // read every line
                while (!sr.EndOfStream)
                {
                    // extract two values
                    var data = sr.ReadLine().Split(';');
                    if (data.Length < 2 || !int.TryParse(data[0], out int x) || !int.TryParse(data[1], out int y))
                        throw new InvalidFileException();
                    // and return them
                    yield return new Point(x, y);
                }
            }
        }

        public void SetSeriesManager(ISeriesManager seriesManager) => this.seriesManager = seriesManager;
    }
}
