﻿using System.IO;

namespace Plotter.CSVPlugin
{
    public class InvalidFileException : IOException
    {
        public InvalidFileException()
        {
        }

        public InvalidFileException(string message)
            : base(message)
        {
        }
    }
}
