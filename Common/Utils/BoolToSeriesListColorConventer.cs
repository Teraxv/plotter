﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace Plotter.Common.Utils
{
    // converter used in PLotTabView.xaml to set the foreground color
    public class BoolToSeriesListColorConventer : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) => (bool)value ? Brushes.Black : Brushes.Gray;
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) => (Brush)value == Brushes.Black;
    }
}
