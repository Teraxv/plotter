﻿using System.Windows;

namespace Plotter.Common.Utils
{
    public static class WindowHelper
    {
        // show new warning window with desired message
        public static void ShowWarning(string message)
        {
            var window = new InfoWindow
            {
                DataContext = new { Content = message }
            };

            var mainWindow = Application.Current.MainWindow;
            if (mainWindow.IsLoaded)
                window.Owner = mainWindow;

            window.Show();
        }
    }
}
