﻿using Plotter.Core.Controls;

namespace Plotter.Common.Controls
{
    public abstract class PanelTabViewModelBase : ViewModelBase
    {
        // force every Tab panel to have a title
        public PanelTabViewModelBase(string title)
        {
            this.title = title;
        }

        private string title;
        public string Title
        {
            get { return title; }
            protected set { Set(ref title, value); }
        }
    }
}
