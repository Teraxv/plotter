﻿using OxyPlot;
using OxyPlot.Series;
using Plotter.Core.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing;
using System.Linq;

namespace Plotter.Common.Models
{
    // restrict to only adding series, so we can pass object of this class whenever we want (ie. to each plugin, so we won't end up with ugly event ladder)
    public class SeriesManager : IReadOnlyCollection<Series>, ISeriesManager, INotifyCollectionChanged
    {
        // 20 series colors in order
        private static readonly OxyColor[] seriesColors = new[] {
            OxyColors.CornflowerBlue, OxyColors.Orange, OxyColors.Lime, OxyColors.Red, OxyColors.Turquoise,
            OxyColors.Olive, OxyColors.Purple, OxyColors.Green, OxyColors.Firebrick, OxyColors.Gold,
            OxyColors.RosyBrown, OxyColors.Pink, OxyColors.Blue, OxyColors.YellowGreen, OxyColors.Khaki,
            OxyColors.DeepPink, OxyColors.DarkOrange, OxyColors.Fuchsia, OxyColors.Honeydew, OxyColors.Maroon
        };

        // composite base
        private readonly ICollection<Series> series;
        
        public event NotifyCollectionChangedEventHandler CollectionChanged;

        public SeriesManager(ICollection<Series> series)
        {
            if (series is null)
                throw new ArgumentNullException("series");
            this.series = series;
        }

        public int Count => series.Count;
        public IEnumerator<Series> GetEnumerator() => series.GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        // we don't want to have oxyplot inside each plugin
        public bool AddSeries(string name, IEnumerable<Point> data)
        {
            return AddSeries(name, data.Select(o => new DataPoint(o.X, o.Y)));
        }

        // add new series with correct serie color
        public bool AddSeries(string name, IEnumerable<DataPoint> data)
        {
            if (data == null || string.IsNullOrWhiteSpace(name) || series.Any(o => o.Title.Equals(name)))
                return false;

            var newSerie = new LineSeries
            {
                Color = seriesColors[series.Count % seriesColors.Length],
                ItemsSource = data.ToList(),    // we want to lock that list tight
                Title = name
            };

            int newIndex = series.Count;
            series.Add(newSerie);

            CollectionChanged?.Invoke(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, newSerie, newIndex));
            return true;
        }

        public bool Contains(string name) => series.Any(o => o.Title.Equals(name));
    }
}
