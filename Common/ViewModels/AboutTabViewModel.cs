﻿using Plotter.Common.Controls;

namespace Plotter.Common.ViewModels
{
    public class AboutTabViewModel : PanelTabViewModelBase
    {
        public AboutTabViewModel()
            : base("About")
        {
        }
    }
}
