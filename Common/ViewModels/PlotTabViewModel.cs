﻿using OxyPlot;
using OxyPlot.Axes;
using Plotter.Common.Controls;
using Plotter.Common.Models;
using System.Diagnostics;

namespace Plotter.Common.ViewModels
{
    public class PlotTabViewModel : PanelTabViewModelBase
    {
        public PlotTabViewModel()
            : base("Plot")
        {
            // populate with default data
            PlotModel = new PlotModel();
            SeriesManager = new SeriesManager(PlotModel.Series);
            PlotModel.Axes.Add(new LinearAxis
            {
                Position = AxisPosition.Bottom,
                Title = "Time"
            });
            PlotModel.Axes.Add(new LinearAxis
            {
                Position = AxisPosition.Left,
                Title = "Value"
            });

            FillWithRandomData();
        }

        public PlotModel PlotModel { get; }
        public SeriesManager SeriesManager { get; }

        [DebuggerStepThrough]
        private void FillWithRandomData()
        {
            SeriesManager.AddSeries("Pierwsza", new[]
            {
                new DataPoint(0, 4),
                new DataPoint(10, 13),
                new DataPoint(20, 15),
                new DataPoint(30, 16),
                new DataPoint(40, 12),
                new DataPoint(50, 12)
            });

            SeriesManager.AddSeries("Druga", new[]
            {
                new DataPoint(0, 15),
                new DataPoint(10, 16),
                new DataPoint(20, 12),
                new DataPoint(30, 10),
                new DataPoint(40, 13),
                new DataPoint(50, 14)
            });

            SeriesManager.AddSeries("Trzecia", new[]
            {
                new DataPoint(0, 6),
                new DataPoint(10, 8),
                new DataPoint(20, 11),
                new DataPoint(30, 9),
                new DataPoint(40, 7),
                new DataPoint(50, 8)
            });
        }
    }
}
