﻿using OxyPlot;
using Plotter.Common.Controls;
using Plotter.Common.Models;
using Plotter.Core.Plugins;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;

namespace Plotter.Common.ViewModels
{
    public delegate void SeriesAddedHandler(object sender, string name, IEnumerable<DataPoint> data);

    public class ImportTabViewModel : PanelTabViewModelBase
    {
        public ImportTabViewModel(SeriesManager seriesManager)
            : base("Import")
        {
            // load all available plugins
            var plugins = new List<IPlugin>();
            foreach (var file in Directory.GetFiles(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "plugins"), "*.dll"))
            {
                var assembly = Assembly.LoadFile(file);
                var pluginType = assembly?.DefinedTypes.SingleOrDefault(o => o.ImplementedInterfaces.Any(x => x.Equals(typeof(IPlugin))));
                var viewType = assembly?.DefinedTypes.SingleOrDefault(o => o.BaseType.Equals(typeof(UserControl)));
                if (pluginType == null || viewType == null)
                    continue;

                // create an instance of plugin class
                IPlugin instance = (IPlugin)Activator.CreateInstance(pluginType);
                
                // add view - viewmodel binding to the resource dictionary
                var template = new DataTemplate(pluginType)
                {
                    VisualTree = new FrameworkElementFactory(viewType, instance.Name)
                };
                Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary() { { new DataTemplateKey(pluginType), template } });

                // inject SeriesManager to the plugin
                instance.SetSeriesManager(seriesManager);
                plugins.Add(instance);

            }
            AvailablePlugins = plugins;
            SelectedPlugin = AvailablePlugins.FirstOrDefault();
        }

        public IList<IPlugin> AvailablePlugins { get; }

        private IPlugin selectedPlugin;
        public IPlugin SelectedPlugin
        {
            get { return selectedPlugin; }
            set { Set(ref selectedPlugin, value); }
        }
    }
}
