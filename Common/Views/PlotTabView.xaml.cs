﻿using OxyPlot.Series;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;

namespace Plotter.Common.Views
{
    public partial class PlotTabView : UserControl
    {
        public PlotTabView()
        {
            InitializeComponent();
        }

        private void Label_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var label = sender as ContentControl;
            var series = label?.DataContext as LineSeries;
            if (e.ChangedButton != MouseButton.Left || series == null)
                return;
            
            series.IsVisible ^= true;
            plot.InvalidatePlot();

            // not beautiful, but it seems that OxyPlot doesn't like DP
            BindingOperations.GetBindingExpression(label, ForegroundProperty).UpdateTarget();
        }
    }
}
