﻿using System.Windows;

namespace Plotter.Common
{
    public partial class InfoWindow : Window
    {
        public InfoWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e) => Close();
    }
}
