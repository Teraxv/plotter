﻿using Plotter.Core.Controls;
using Plotter.Core.Models;
using Plotter.Core.Plugins;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Input;

namespace RandomPlugin
{
    public class RandomPluginViewModel : ViewModelBase, IPlugin
    {
        // a bunch of useful consts, magic numbers are big no-no
        private const int MAX_POINTS_IN_SERIES = 1000;
        private const int MAX_Y = 40;
        private const int MIN_Y = 0;

        private ISeriesManager seriesManager;

        public RandomPluginViewModel()
        {
        }

        public string Name => "Random";

        private string seriesName;
        public string SeriesName
        {
            get { return seriesName; }
            set { Set(ref seriesName, value); }
        }

        private string sointsCount;
        public string PointsCount
        {
            get { return sointsCount; }
            set { Set(ref sointsCount, value); }
        }
        
        private ICommand addCommand;
        public ICommand AddCommand
        {
            get
            {
                return addCommand ?? (addCommand = new BasicCommand(() =>
                {
                    int count;
                    if (!int.TryParse(PointsCount, out count) || count < 1 || count > MAX_POINTS_IN_SERIES
                        || string.IsNullOrWhiteSpace(seriesName) || seriesManager.Contains(seriesName))
                        return;

                    seriesManager.AddSeries(seriesName, GetRandomData(count));
                    PointsCount = string.Empty;
                    SeriesName = string.Empty;
                }));
            }
        }

        private IEnumerable<Point> GetRandomData(int count)
        {
            var rand = new Random();
            // set starting point at the middle
            int y = MIN_Y + (MAX_Y - MIN_Y) / 2;
            for(int x = 0; x < count; x++)
            {
                yield return new Point(x, y);
                // make sure that MAX_Y > y > MIN_Y
                y = Math.Min(MAX_Y, Math.Max(MIN_Y, y + rand.Next(-3, 3)));
            }
        }

        public void SetSeriesManager(ISeriesManager seriesManager) => this.seriesManager = seriesManager;
    }
}
