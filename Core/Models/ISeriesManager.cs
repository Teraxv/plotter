﻿using System.Collections.Generic;
using System.Drawing;

namespace Plotter.Core.Models
{
    // small hack to operate on SeriesManager inside plugins
    public interface ISeriesManager
    {
        bool AddSeries(string name, IEnumerable<Point> data);
        bool Contains(string name);
    }
}
