﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace Plotter.Core.Controls
{
    public abstract class ViewModelBase : INotifyPropertyChanged, IDisposable
    {
        private bool disposed = false;

        public ViewModelBase()
        {
            ThrowOnInvalidPropertyName = false;
        }

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            VerifyPropertyName(propertyName);
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        protected void RaisePropertyChanged()
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(null));
        }

        // a way for fast property changed notification propagation
        protected bool Set<T>(ref T field, T value, [CallerMemberName]string propertyName = null)
        {
            if ((typeof(T).IsValueType && field.Equals(value)) || ReferenceEquals(field, value))
            {
                return false;
            }

            field = value;
            RaisePropertyChanged(propertyName);
            return true;
        }

        #endregion

        #region IDisposable

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (disposed)
            {
                return;
            }

            if (disposing)
            {
                Disposing();
            }

            disposed = true;
        }

        protected virtual void Disposing()
        {
        }

        #endregion // IDisposable

        protected virtual bool ThrowOnInvalidPropertyName { get; private set; }

        // check if we raise event on existing property, useful for debugging purposes
        [Conditional("DEBUG")]
        [DebuggerStepThrough]
        private void VerifyPropertyName(string propertyName)
        {
            if (TypeDescriptor.GetProperties(this)[propertyName] == null)
            {
                string msg = "Invalid property name: " + propertyName;
                if (ThrowOnInvalidPropertyName)
                    throw new Exception(msg);
                Debug.Fail(msg);
            }
        }
    }
}
