﻿using System;
using System.Windows.Input;

namespace Plotter.Core.Controls
{
    // very basic ICommand implementation, parameterless and with parameter
    public class BasicCommand : ICommand
    {
        private readonly Action execute;
        public event EventHandler CanExecuteChanged;

        public BasicCommand(Action execute)
        {
            this.execute = execute;
        }

        public bool CanExecute(object parameter) => true;
        public void Execute(object parameter) => execute();
    }

    public class BasicCommand<T> : ICommand
    {
        private readonly Action<T> execute;
        public event EventHandler CanExecuteChanged;

        public BasicCommand(Action<T> execute)
        {
            this.execute = execute;
        }

        public bool CanExecute(object parameter) => true;
        public void Execute(object parameter) => execute((T)parameter);
    }
}
