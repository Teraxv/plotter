﻿using Plotter.Core.Models;

namespace Plotter.Core.Plugins
{
    public interface IPlugin
    {
        string Name { get; }
        void SetSeriesManager(ISeriesManager seriesManager);
    }
}
